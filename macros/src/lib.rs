use itertools::Itertools;
use proc_macro::TokenStream;
use quote::{format_ident, quote};
use syn::{parse::Parser, punctuated::Punctuated, ExprLit, Token};

#[proc_macro]
pub fn match_solutions(_: TokenStream) -> TokenStream {
    let entries = std::fs::read_dir("src/solutions")
        .unwrap()
        .flatten()
        .collect::<Vec<_>>();

    let modules = {
        let modules = entries
            .iter()
            .map(|entry| entry.file_name())
            .filter_map(|file_name| {
                let module_name = file_name.to_str()?.strip_suffix(".rs")?;
                Some(format_ident!("{}", module_name))
            });

        quote!(
            #(mod #modules);*;
        )
    };

    let functions = {
        let functions = entries
            .iter()
            .map(|entry| entry.file_name())
            .filter_map(|file_name| {
                let day = file_name.to_str()?.strip_suffix(".rs")?.strip_prefix('s')?;
                Some(day.to_owned())
            })
            .cartesian_product(["1", "2"])
            .map(|(mut day, part)| {
                let day_ident = format_ident!("s{day}");
                let part_function_ident = format_ident!("part_{part}");
                day.push('_');
                day.push_str(part);
                quote!(#day => #day_ident::#part_function_ident(input))
            });

        quote!(
            pub fn solve(day: &str, input: &str) -> String {
                match day {
                    #(#functions),*,
                    _ => panic!("no solution found for {day}"),
                }
            }
        )
    };

    quote!(
        #modules

        #functions
    )
    .into()
}

#[proc_macro]
pub fn bench(input: TokenStream) -> TokenStream {
    let mut expected = Punctuated::<ExprLit, Token![,]>::parse_terminated
        .parse(input)
        .unwrap()
        .into_iter();

    let (expected_1, expected_2) = (expected.next().unwrap(), expected.next().unwrap());

    quote!(
        extern crate test;

        #[cfg(test)]
        mod tests {
            use super::*;
            use test::Bencher;

            #[bench]
            fn bench_part_1(b: &mut Bencher) {
                let input = std::fs::read_to_string("input").unwrap();
                b.iter(|| part_1(&input));
                assert_eq!(#expected_1.to_string(), part_1(&input));
            }

            #[bench]
            fn bench_part_2(b: &mut Bencher) {
                let input = std::fs::read_to_string("input").unwrap();
                b.iter(|| part_2(&input));
                assert_eq!(#expected_2.to_string(), part_2(&input));
            }
        }
    )
    .into()
}
