#![feature(test)]

use solutions::solve;

mod solutions;

fn main() {
    let day = std::env::args()
        .nth(1)
        .expect("Argument 1 should be a day and part, separated by an underscore (e.g. 1_2)");

    env_logger::Builder::default()
        .filter_module("advent_of_code_2023", log::LevelFilter::Trace)
        .init();

    let input =
        std::fs::read_to_string("input").expect(r#""./input" should exist and be readable"#);

    std::fs::write("output", solve(&day, &input)).expect(r#""output should be writable"#);
}
