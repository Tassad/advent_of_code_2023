use std::collections::HashMap;

use macros::bench;

pub fn part_1(input: &str) -> String {
    let instructions = input.split("\n\n").next().unwrap().chars().cycle();
    let node_lines = input.split("\n\n").nth(1).unwrap().lines();

    let mut nodes = HashMap::<&str, (&str, &str)>::with_capacity(800);
    for line in node_lines {
        let mut split_line = line.split_ascii_whitespace();

        let name = split_line.next().unwrap();
        let left = &split_line.nth(1).unwrap()[1..4];
        let right = &split_line.next().unwrap()[0..3];

        nodes.insert(name, (left, right));
    }

    let mut steps = 0_u32;
    let mut node = nodes.get_key_value("AAA").unwrap();
    for instruction in instructions {
        node = match instruction {
            'L' => nodes.get_key_value(node.1 .0).unwrap(),
            'R' => nodes.get_key_value(node.1 .1).unwrap(),
            _ => panic!("{instruction} is not a valid instruction"),
        };

        steps += 1;

        if node.0 == &"ZZZ" {
            break;
        }
    }

    steps.to_string()
}

pub fn part_2(input: &str) -> String {
    let instructions = input.split("\n\n").next().unwrap().chars().cycle();
    let node_lines = input.split("\n\n").nth(1).unwrap().lines();

    let mut node_directions = HashMap::<&str, (&str, &str)>::with_capacity(800);
    let mut nodes = Vec::new();
    for line in node_lines {
        let mut split_line = line.split_ascii_whitespace();

        let name = split_line.next().unwrap();
        let left = &split_line.nth(1).unwrap()[1..4];
        let right = &split_line.next().unwrap()[0..3];

        node_directions.insert(name, (left, right));
        if name.ends_with('A') {
            nodes.push(Some((name, (left, right))));
        }
    }

    let mut steps = 0_u64;
    let mut finished_nodes = Vec::new();
    for instruction in instructions {
        for node in nodes.iter_mut().filter(|node| node.is_some()) {
            *node = match instruction {
                'L' => Some((node.unwrap().1 .0, node_directions[node.unwrap().1 .0])),
                'R' => Some((node.unwrap().1 .1, node_directions[node.unwrap().1 .1])),
                _ => panic!("{instruction} is not a valid instruction"),
            };
        }

        steps += 1;

        for node in nodes
            .iter_mut()
            .filter(|node| node.is_some() && node.unwrap().0.ends_with('Z'))
        {
            finished_nodes.push(steps);
            *node = None;
        }

        if nodes.iter().all(|node| node.is_none()) {
            break;
        }
    }

    finished_nodes.into_iter().reduce(lcm).unwrap().to_string()
}

fn gcd(a: u64, b: u64) -> u64 {
    if b == 0 {
        a
    } else {
        gcd(b, a % b)
    }
}

fn lcm(a: u64, b: u64) -> u64 {
    a / gcd(a, b) * b
}

bench!(16697, 10668805667831_u64);
