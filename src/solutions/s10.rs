use std::collections::HashSet;

use macros::bench;

pub fn part_1(input: &str) -> String {
    let grid = Grid::new(input);

    let (start_index, _) = grid
        .chars
        .iter()
        .enumerate()
        .find(|(_, &c)| c == 'S')
        .unwrap();
    let (start_x, start_y) = (
        start_index % grid.line_offset,
        start_index / grid.line_offset,
    );

    let (mut next_x, mut next_y) = {
        match grid.get(start_x, start_y.saturating_sub(1)) {
            Some('|') | Some('7') | Some('F') => (start_x, start_y.saturating_sub(1)),
            _ => match grid.get(start_x + 1, start_y) {
                Some('-') | Some('J') | Some('7') => (start_x + 1, start_y),
                _ => match grid.get(start_x, start_y + 1) {
                    Some('|') | Some('L') | Some('J') => (start_x, start_y + 1),
                    _ => panic!("starting point has fewer than 2 connections"),
                },
            },
        }
    };

    let mut steps: u32 = 0;
    let (mut prev_x, mut prev_y) = (start_x, start_y);
    while grid.get(next_x, next_y) != Some(&'S') {
        let direction = Direction::new(prev_x, prev_y, next_x, next_y);
        let next_pipe = grid.get(next_x, next_y).unwrap();
        (prev_x, prev_y) = (next_x, next_y);

        (next_x, next_y) = match direction {
            Direction::Up => match next_pipe {
                '|' => (next_x, next_y - 1),
                '7' => (next_x - 1, next_y),
                'F' => (next_x + 1, next_y),
                _ => unreachable!(),
            },
            Direction::Left => match next_pipe {
                '-' => (next_x - 1, next_y),
                'L' => (next_x, next_y - 1),
                'F' => (next_x, next_y + 1),
                _ => unreachable!(),
            },
            Direction::Right => match next_pipe {
                '-' => (next_x + 1, next_y),
                'J' => (next_x, next_y - 1),
                '7' => (next_x, next_y + 1),
                _ => unreachable!(),
            },
            Direction::Down => match next_pipe {
                '|' => (next_x, next_y + 1),
                'L' => (next_x + 1, next_y),
                'J' => (next_x - 1, next_y),
                _ => unreachable!(),
            },
        };

        steps += 1;
    }

    steps.div_ceil(2).to_string()
}

pub fn part_2(input: &str) -> String {
    let mut grid = Grid::new(input);

    let (start_index, _) = grid
        .chars
        .iter()
        .enumerate()
        .find(|(_, &c)| c == 'S')
        .unwrap();
    let (start_x, start_y) = (
        start_index % grid.line_offset,
        start_index / grid.line_offset,
    );

    let (mut next_x, mut next_y, new_pipe) = {
        let mut directions = Vec::with_capacity(2);

        let up = (start_x, start_y.saturating_sub(1));
        let left = (start_x.saturating_sub(1), start_y);
        let right = (start_x + 1, start_y);
        let down = (start_x, start_y + 1);

        if matches!(grid.get(up.0, up.1), Some('|') | Some('7') | Some('F')) {
            directions.push(Direction::Up);
        }
        if matches!(grid.get(left.0, left.1), Some('-') | Some('L') | Some('F')) {
            directions.push(Direction::Left);
        }
        if matches!(
            grid.get(right.0, right.1),
            Some('-') | Some('J') | Some('7')
        ) {
            directions.push(Direction::Right);
        }
        if matches!(grid.get(down.0, down.1), Some('|') | Some('L') | Some('J')) {
            directions.push(Direction::Down);
        }

        assert!(
            directions.len() == 2,
            "starting point must have exactly 2 connections"
        );

        directions.sort_unstable();
        match directions[..] {
            [Direction::Up, Direction::Left] => (up.0, up.1, 'J'),
            [Direction::Up, Direction::Right] => (up.0, up.1, 'L'),
            [Direction::Up, Direction::Down] => (up.0, up.1, '|'),
            [Direction::Left, Direction::Right] => (left.0, left.1, '-'),
            [Direction::Left, Direction::Down] => (left.0, left.1, '7'),
            [Direction::Right, Direction::Down] => (right.0, right.1, 'F'),
            _ => unreachable!(),
        }
    };

    *grid.get_mut(start_x, start_y).unwrap() = new_pipe;

    let (mut prev_x, mut prev_y) = (start_x, start_y);
    let mut loop_points = HashSet::from([(start_x, start_y), (next_x, next_y)]);
    while (next_x, next_y) != (start_x, start_y) {
        let direction = Direction::new(prev_x, prev_y, next_x, next_y);
        let next_pipe = grid.get(next_x, next_y).unwrap();
        (prev_x, prev_y) = (next_x, next_y);

        (next_x, next_y) = match direction {
            Direction::Up => match next_pipe {
                '|' => (next_x, next_y - 1),
                '7' => (next_x - 1, next_y),
                'F' => (next_x + 1, next_y),
                _ => unreachable!(),
            },
            Direction::Left => match next_pipe {
                '-' => (next_x - 1, next_y),
                'L' => (next_x, next_y - 1),
                'F' => (next_x, next_y + 1),
                _ => unreachable!(),
            },
            Direction::Right => match next_pipe {
                '-' => (next_x + 1, next_y),
                'J' => (next_x, next_y - 1),
                '7' => (next_x, next_y + 1),
                _ => unreachable!(),
            },
            Direction::Down => match next_pipe {
                '|' => (next_x, next_y + 1),
                'L' => (next_x + 1, next_y),
                'J' => (next_x - 1, next_y),
                _ => unreachable!(),
            },
        };

        loop_points.insert((next_x, next_y));
    }

    let mut inside_count = 0;
    for (i, _) in grid.chars.iter().enumerate() {
        let (x, y) = (i % grid.line_offset, i / grid.line_offset);

        if loop_points.contains(&(x, y)) {
            continue;
        }

        let mut inside = false;
        let mut seen_up_right = false;
        let mut seen_down_right = false;
        for j in x..grid.line_offset {
            if loop_points.contains(&(j, y)) {
                match grid.get(j, y) {
                    Some('|') => inside = !inside,
                    Some('L') => {
                        seen_down_right = false;
                        seen_up_right = true;
                    }
                    Some('J') => {
                        if seen_down_right {
                            seen_down_right = false;
                            inside = !inside;
                        }
                        seen_up_right = false;
                    }
                    Some('F') => {
                        seen_up_right = false;
                        seen_down_right = true;
                    }
                    Some('7') => {
                        if seen_up_right {
                            seen_up_right = false;
                            inside = !inside;
                        }
                        seen_down_right = false;
                    }
                    Some('-') => (),
                    _ => {
                        seen_up_right = false;
                        seen_down_right = false;
                    }
                };
            }
        }

        if inside {
            #[cfg(debug_assertions)]
            ::log::debug!("({x}, {y}) is inside!");
            inside_count += 1;
        }
    }

    inside_count.to_string()
}

struct Grid {
    chars: Vec<char>,
    line_offset: usize,
}

impl Grid {
    fn new(s: &str) -> Self {
        let mut lines = s.lines().peekable();
        let line_offset = lines.peek().map_or(0, |line| line.len());

        Self {
            chars: lines.flat_map(|line| line.chars()).collect(),
            line_offset,
        }
    }

    fn get(&self, x: usize, y: usize) -> Option<&char> {
        self.chars.get(y * self.line_offset + x)
    }

    fn get_mut(&mut self, x: usize, y: usize) -> Option<&mut char> {
        self.chars.get_mut(y * self.line_offset + x)
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord)]
enum Direction {
    Up,
    Left,
    Right,
    Down,
}

impl Direction {
    fn new(prev_x: usize, prev_y: usize, next_x: usize, next_y: usize) -> Self {
        if let Some(n) = next_x.checked_sub(prev_x) {
            if n > 0 {
                return Self::Right;
            }
        } else {
            return Self::Left;
        }

        if let Some(n) = next_y.checked_sub(prev_y) {
            if n > 0 {
                return Self::Down;
            }
        } else {
            return Self::Up;
        }

        unreachable!()
    }
}

#[cfg(test)]
mod sample_tests {
    use super::*;

    const SAMPLE_INPUTS: [&str; 7] = [
        "\
.....
.S-7.
.|.|.
.L-J.
.....
",
        "\
-L|F7
7S-7|
L|7||
-L-J|
L|-JF
",
        "\
..F7.
.FJ|.
SJ.L7
|F--J
LJ...
",
        "\
7-F7-
.FJ|7
SJLL7
|F--J
LJ.LJ
",
        "\
...........
.S-------7.
.|F-----7|.
.||.....||.
.||.....||.
.|L-7.F-J|.
.|..|.|..|.
.L--J.L--J.
...........
",
        "\
.F----7F7F7F7F-7....
.|F--7||||||||FJ....
.||.FJ||||||||L7....
FJL7L7LJLJ||LJ.L-7..
L--J.L7...LJS7F-7L7.
....F-J..F7FJ|L7L7L7
....L7.F7||L7|.L7L7|
.....|FJLJ|FJ|F7|.LJ
....FJL-7.||.||||...
....L---J.LJ.LJLJ...
",
        "\
FF7FSF7F7F7F7F7F---7
L|LJ||||||||||||F--J
FL-7LJLJ||||||LJL-77
F--JF--7||LJLJ7F7FJ-
L---JF-JLJ.||-FJLJJ7
|F|F-JF---7F7-L7L|7|
|FFJF7L7F-JF7|JL---7
7-L-JL7||F7|L7F-7F7|
L.L7LFJ|||||FJL7||LJ
L7JLJL-JLJLJL--JLJ.L
",
    ];

    #[test]
    fn part_1_works_on_samples() {
        assert_eq!(part_1(SAMPLE_INPUTS[0]), "4");
        assert_eq!(part_1(SAMPLE_INPUTS[1]), "4");
        assert_eq!(part_1(SAMPLE_INPUTS[2]), "8");
        assert_eq!(part_1(SAMPLE_INPUTS[3]), "8");
    }

    #[test]
    fn part_2_works_on_samples() {
        assert_eq!(part_2(SAMPLE_INPUTS[4]), "4");
        assert_eq!(part_2(SAMPLE_INPUTS[5]), "8");
        assert_eq!(part_2(SAMPLE_INPUTS[6]), "10");
    }
}

bench!(6864, 349);
