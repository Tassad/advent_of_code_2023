use std::collections::HashSet;

use macros::bench;

pub fn part_1(input: &str) -> String {
    let mut numbers = Vec::new();
    let mut symbol_positions = Vec::new();

    for line in input.lines().enumerate() {
        let (y, chars) = (line.0, line.1.char_indices());

        let mut num = String::default();
        for (x, char) in chars {
            if !char.is_ascii_digit() {
                if !num.is_empty() {
                    numbers.push((x - num.len(), y, num));
                    num = String::default();
                }

                if char != '.' {
                    symbol_positions.push((x, y));
                }
            } else {
                num.push(char);
            }
        }

        if !num.is_empty() {
            numbers.push((line.1.len() - num.len(), y, num));
        }
    }

    let mut sum: u32 = 0;
    'num_loop: for (x, y, num) in numbers {
        let start_x = x.saturating_sub(1);
        let end_x = x + num.len();

        let start_y = y.saturating_sub(1);
        let end_y = y + 1;

        for pos_x in start_x..=end_x {
            for pos_y in start_y..=end_y {
                if symbol_positions.contains(&(pos_x, pos_y)) {
                    sum += num.parse::<u32>().unwrap();
                    continue 'num_loop;
                }
            }
        }
    }

    sum.to_string()
}

pub fn part_2(input: &str) -> String {
    let mut numbers = Vec::new();
    let mut gear_positions = Vec::new();

    for line in input.lines().enumerate() {
        let (y, chars) = (line.0, line.1.char_indices());

        let mut num = String::default();
        for (x, char) in chars {
            if !char.is_ascii_digit() {
                if !num.is_empty() {
                    numbers.push(((x - num.len())..x, y, num));
                    num = String::default();
                }

                if char == '*' {
                    gear_positions.push((x, y));
                }
            } else {
                num.push(char);
            }
        }

        if !num.is_empty() {
            numbers.push(((line.1.len() - num.len())..(line.1.len()), y, num));
        }
    }

    let mut adjacent_gear_numbers = HashSet::new();
    'num_loop: for (x_range, y, num) in numbers {
        let start_x = x_range.start.saturating_sub(1);
        let end_x = x_range.end;

        let start_y = y.saturating_sub(1);
        let end_y = y + 1;

        for pos_x in start_x..=end_x {
            for pos_y in start_y..=end_y {
                if gear_positions.contains(&(pos_x, pos_y)) {
                    adjacent_gear_numbers.insert((x_range, y, num));
                    continue 'num_loop;
                }
            }
        }
    }

    let mut sum: u64 = 0;
    for (x, y) in gear_positions {
        let start_x = x.saturating_sub(1);
        let end_x = x + 1;

        let start_y = y.saturating_sub(1);
        let end_y = y + 1;

        let mut adjacent = HashSet::new();
        for pos_x in start_x..=end_x {
            for pos_y in start_y..=end_y {
                for (x_range, y, num) in adjacent_gear_numbers.iter() {
                    if x_range.contains(&pos_x) && pos_y == *y {
                        adjacent.insert(num);
                    }
                }
            }
        }

        if adjacent.len() != 2 {
            continue;
        }
        sum += adjacent
            .iter()
            .map(|s| s.parse::<u64>().unwrap())
            .product::<u64>();
    }

    sum.to_string()
}

bench!(532331, 82301120);
