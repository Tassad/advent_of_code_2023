use std::collections::HashMap;

use indexmap::IndexMap;

use macros::bench;

pub fn part_1(input: &str) -> String {
    input
        .split(',')
        .map(|s| {
            s.trim_end()
                .chars()
                .fold(0, |acc, c| (acc + c as u32) * 17 % 256)
        })
        .sum::<u32>()
        .to_string()
}

pub fn part_2(input: &str) -> String {
    let mut boxes: HashMap<u8, IndexMap<&str, &str>> = HashMap::new();
    for step in input.split(',') {
        let box_n = step
            .trim_end()
            .chars()
            .take_while(char::is_ascii_alphabetic)
            .fold(0, |acc, c| (acc + c as u32) * 17 % 256)
            .try_into()
            .unwrap();
        let (label, foc_len) = if step.contains('=') {
            let split = step.trim_end().split_once('=').unwrap();
            (split.0, Some(split.1))
        } else {
            let i = step.find('-').unwrap();
            (&step[0..i], None)
        };

        let entry = boxes.entry(box_n).or_default();
        match foc_len {
            Some(foc_len) => entry.insert(label, foc_len),
            None => entry.shift_remove(label),
        };
    }

    boxes
        .into_iter()
        .map(|(box_n, contents)| {
            contents
                .into_iter()
                .enumerate()
                .map(|(i, (_, foc_len))| {
                    (box_n as usize + 1) * (i + 1) * foc_len.parse::<usize>().unwrap()
                })
                .sum::<usize>()
        })
        .sum::<usize>()
        .to_string()
}

#[cfg(test)]
mod sample_tests {
    use super::*;

    const SAMPLE_INPUTS: [&str; 1] = ["rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7
    "];

    #[test]
    fn part_1_works_on_samples() {
        assert_eq!(part_1(SAMPLE_INPUTS[0]), "1320");
    }

    #[test]
    fn part_2_works_on_samples() {
        assert_eq!(part_2(SAMPLE_INPUTS[0]), "145");
    }
}

bench!(511498, 284674);
