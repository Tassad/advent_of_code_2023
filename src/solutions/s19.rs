use std::collections::HashMap;

use macros::bench;

pub fn part_1(input: &str) -> String {
    let mut split = input.split("\n\n");
    let workflows = split
        .next()
        .unwrap()
        .lines()
        .map(|line| {
            let mut split = line.split('{');
            (
                split.next().unwrap(),
                split
                    .next()
                    .unwrap()
                    .strip_suffix('}')
                    .unwrap()
                    .split(',')
                    .collect::<Vec<_>>(),
            )
        })
        .collect::<HashMap<_, _>>();
    let parts = split.next().unwrap();

    parts
        .lines()
        .fold(0, |acc, line| {
            let part = Part::new(line);
            let mut next_step = "in";
            let accepted = 'outer: loop {
                for step in workflows[next_step].iter() {
                    let lt_i = step.find('<');
                    let gt_i = step.find('>');
                    if lt_i.is_none() && gt_i.is_none() {
                        match *step {
                            "A" => {
                                break 'outer true;
                            }
                            "R" => {
                                break 'outer false;
                            }
                            other => {
                                next_step = other;
                                continue 'outer;
                            }
                        }
                    } else {
                        let part_val = match &step[0..=0] {
                            "x" => part.x,
                            "m" => part.m,
                            "a" => part.a,
                            "s" => part.s,
                            other => unreachable!("this part of input must not contain {other}"),
                        };
                        let colon_i = step.find(':').unwrap();
                        let mut condition_passed = false;
                        if let Some(i) = lt_i {
                            if part_val < step[i + 1..colon_i].parse().unwrap() {
                                condition_passed = true;
                            }
                        } else if let Some(i) = gt_i {
                            if part_val > step[i + 1..colon_i].parse().unwrap() {
                                condition_passed = true;
                            }
                        }

                        if condition_passed {
                            match &step[colon_i + 1..] {
                                "A" => {
                                    break 'outer true;
                                }
                                "R" => {
                                    break 'outer false;
                                }
                                other => {
                                    next_step = other;
                                    continue 'outer;
                                }
                            }
                        }
                    }
                }
            };

            if accepted {
                acc + part.x + part.m + part.a + part.s
            } else {
                acc
            }
        })
        .to_string()
}

pub fn part_2(input: &str) -> String {
    todo!()
}

struct Part {
    x: u32,
    m: u32,
    a: u32,
    s: u32,
}

impl Part {
    fn new(s: &str) -> Self {
        let mut split = s.split(',');

        let x = split
            .next()
            .unwrap()
            .chars()
            .skip_while(|c| !c.is_ascii_digit())
            .collect::<String>()
            .parse()
            .unwrap();
        let m = split
            .next()
            .unwrap()
            .chars()
            .skip_while(|c| !c.is_ascii_digit())
            .collect::<String>()
            .parse()
            .unwrap();
        let a = split
            .next()
            .unwrap()
            .chars()
            .skip_while(|c| !c.is_ascii_digit())
            .collect::<String>()
            .parse()
            .unwrap();
        let s = split
            .next()
            .unwrap()
            .chars()
            .skip_while(|c| !c.is_ascii_digit())
            .take_while(char::is_ascii_digit)
            .collect::<String>()
            .parse()
            .unwrap();

        Self { x, m, a, s }
    }
}

#[cfg(test)]
mod sample_tests {
    use super::*;

    const SAMPLE_INPUTS: [&str; 1] = ["\
px{a<2006:qkq,m>2090:A,rfg}
pv{a>1716:R,A}
lnx{m>1548:A,A}
rfg{s<537:gd,x>2440:R,A}
qs{s>3448:A,lnx}
qkq{x<1416:A,crn}
crn{x>2662:A,R}
in{s<1351:px,qqz}
qqz{s>2770:qs,m<1801:hdj,R}
gd{a>3333:R,R}
hdj{m>838:A,pv}

{x=787,m=2655,a=1222,s=2876}
{x=1679,m=44,a=2067,s=496}
{x=2036,m=264,a=79,s=2244}
{x=2461,m=1339,a=466,s=291}
{x=2127,m=1623,a=2188,s=1013}
"];

    #[test]
    fn part_1_works_on_samples() {
        assert_eq!(part_1(SAMPLE_INPUTS[0]), "19114");
    }

    #[test]
    fn part_2_works_on_samples() {
        assert_eq!(part_2(SAMPLE_INPUTS[0]), "167409079868000");
    }
}

// bench!(353553,);
