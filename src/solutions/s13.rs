use macros::bench;

pub fn part_1(input: &str) -> String {
    input
        .split("\n\n")
        .map(|part| {
            let grid = Grid::new(part);

            let mut col_offset = 0;
            let cols = (0..grid.row_offset)
                .map(|x| grid.col_iter(x))
                .inspect(|col| {
                    if col_offset == 0 {
                        col_offset = col.clone().count();
                    }
                })
                .map(|col| col.collect::<String>().into_boxed_str())
                .collect::<String>()
                .into_boxed_str();

            let mut row_iter = grid.rows_iter().enumerate().peekable();
            let mut mirrored_rows: usize = 0;
            while let Some((i, row)) = row_iter.next() {
                if mirrored_rows > 0 {
                    if rowcol_at(
                        &grid.rows,
                        grid.row_offset,
                        grid.rows.len() / grid.row_offset - i,
                    )
                    .map_or(true, |other_row| row == other_row)
                    {
                        mirrored_rows += 1;
                        continue;
                    } else {
                        break;
                    }
                }

                if row_iter
                    .peek()
                    .is_some_and(|(_, other_row)| &row == other_row)
                {
                    mirrored_rows += 1;
                    row_iter.next();
                }
            }

            let mut col_iter = cols_iter(&cols, col_offset).enumerate().peekable();
            let mut mirrored_cols: usize = 0;
            while let Some((i, col)) = col_iter.next() {
                if mirrored_cols > 0 {
                    if rowcol_at(&cols, col_offset, cols.len() / col_offset - i)
                        .map_or(true, |other_col| col == other_col)
                    {
                        mirrored_cols += 1;
                        continue;
                    } else {
                        break;
                    }
                }

                if col_iter
                    .peek()
                    .is_some_and(|(_, other_col)| &col == other_col)
                {
                    mirrored_cols += 1;
                    col_iter.next();
                }
            }

            if grid.rows.len() / grid.row_offset % 2 != 0 {
                mirrored_rows += 1;
            }
            if cols.len() / col_offset % 2 != 0 {
                mirrored_cols += 1;
            }

            if mirrored_rows >= mirrored_cols {
                100 * mirrored_rows
            } else {
                mirrored_cols
            }
        })
        .sum::<usize>()
        .to_string()
}

pub fn part_2(input: &str) -> String {
    todo!();
}

fn cols_iter(cols: &str, col_offset: usize) -> impl Iterator<Item = &str> {
    let mut i = 0;
    ::std::iter::from_fn(move || {
        i += 1;
        if (i - 1) * col_offset < cols.len() {
            Some(&cols[(i - 1) * col_offset..i * col_offset])
        } else {
            None
        }
    })
}

fn rowcol_at(data: &str, data_offset: usize, i: usize) -> Option<&str> {
    data.get(i * data_offset..(i + 1) * data_offset)
}

struct Grid {
    rows: Box<str>,
    row_offset: usize,
}

impl Grid {
    fn new(s: &str) -> Self {
        let mut lines = s.lines().peekable();

        let row_offset = lines.peek().map_or(0, |line| line.len());
        let rows = lines.collect::<String>().into_boxed_str();

        Self { rows, row_offset }
    }

    // fn get(&self, x: usize, y: usize) -> Option<&u8> {
    //     self.points.get(y * self.line_offset + x)
    // }

    // fn get_mut(&mut self, x: usize, y: usize) -> Option<&mut u8> {
    //     self.points.get_mut(y * self.line_offset + x)
    // }

    // fn get_adjacent(&self, x: usize, y: usize) -> Vec<(usize, usize)> {
    //     let mut out = vec![];

    //     for x in [x.checked_sub(1), Some(x + 1)].into_iter().flatten() {
    //         out.push((x, y));
    //     }

    //     for y in [y.checked_sub(1), Some(y + 1)].into_iter().flatten() {
    //         out.push((x, y));
    //     }

    //     out
    // }

    fn rows_iter(&self) -> impl Iterator<Item = &str> {
        let mut i = 0;
        ::std::iter::from_fn(move || {
            i += 1;
            if (i - 1) * self.row_offset < self.rows.len() {
                Some(&self.rows[(i - 1) * self.row_offset..i * self.row_offset])
            } else {
                None
            }
        })
    }

    fn col_iter(&self, x: usize) -> impl Iterator<Item = &str> + Clone {
        let mut i = 0;
        ::std::iter::from_fn(move || {
            i += 1;
            if (i - 1) * self.row_offset < self.rows.len() {
                Some(&self.rows[x + (i - 1) * self.row_offset..=x + (i - 1) * self.row_offset])
            } else {
                None
            }
        })
    }
}

impl ::std::fmt::Display for Grid {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for (i, char) in self.rows.chars().enumerate() {
            write!(f, "{char}")?;
            if i % self.row_offset == self.row_offset - 1 {
                writeln!(f)?;
            }
        }

        Ok(())
    }
}

#[cfg(test)]
mod sample_tests {
    use super::*;

    const SAMPLE_INPUTS: [&str; 1] = ["\
#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.

#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#
"];

    #[test]
    fn part_1_works_on_samples() {
        assert_eq!(part_1(SAMPLE_INPUTS[0]), "405");
    }

    #[test]
    fn part_2_works_on_samples() {
        todo!();
    }
}

// bench!(,);
