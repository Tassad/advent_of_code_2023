use std::cmp::Ordering;

use itertools::Itertools;

use macros::bench;

pub fn part_1(input: &str) -> String {
    input
        .lines()
        .map(|line| {
            let (hand, bid) = line.split_ascii_whitespace().collect_tuple().unwrap();
            (P1Hand::new(hand), bid.parse::<u32>().unwrap())
        })
        .sorted_by(|a, b| a.0.cmp(&b.0))
        .map(|(_, bid)| bid)
        .enumerate()
        .fold(0_u32, |acc, (i, bid)| acc + (i + 1) as u32 * bid)
        .to_string()
}

pub fn part_2(input: &str) -> String {
    input
        .lines()
        .map(|line| {
            let (hand, bid) = line.split_ascii_whitespace().collect_tuple().unwrap();
            (P2Hand::new(hand), bid.parse::<u32>().unwrap())
        })
        .sorted_by(|a, b| a.0.cmp(&b.0))
        .map(|(_, bid)| bid)
        .enumerate()
        .fold(0_u32, |acc, (i, bid)| acc + (i + 1) as u32 * bid)
        .to_string()
}

#[derive(Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
enum P1Card {
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
    Ace,
}

#[derive(Debug)]
struct P1Hand {
    hand: Box<[P1Card]>,
    hand_type: HandType,
}

impl P1Hand {
    fn new(str: &str) -> Self {
        if str.len() != 5 {
            panic!("Hands must contain exactly 5 cards");
        }

        let hand = str
            .chars()
            .map(|c| match c {
                'A' => P1Card::Ace,
                'K' => P1Card::King,
                'Q' => P1Card::Queen,
                'J' => P1Card::Jack,
                'T' => P1Card::Ten,
                '9' => P1Card::Nine,
                '8' => P1Card::Eight,
                '7' => P1Card::Seven,
                '6' => P1Card::Six,
                '5' => P1Card::Five,
                '4' => P1Card::Four,
                '3' => P1Card::Three,
                '2' => P1Card::Two,
                _ => panic!("{c} is not a valid card"),
            })
            .collect::<Vec<_>>()
            .into_boxed_slice();
        let hand_type = P1Hand::get_type(&hand);

        Self { hand, hand_type }
    }

    fn get_type(cards: &[P1Card]) -> HandType {
        let counts = cards.iter().counts();

        match counts.values().max().unwrap() {
            5 => HandType::Five,
            4 => HandType::Four,
            1 => HandType::High,
            3 => {
                if counts.len() == 2 {
                    HandType::FullHouse
                } else {
                    HandType::Three
                }
            }
            2 => {
                if counts.len() == 3 {
                    HandType::TwoPair
                } else {
                    HandType::OnePair
                }
            }
            _ => panic!("couldn't get hand type for {:?}", counts),
        }
    }
}

impl PartialEq for P1Hand {
    fn eq(&self, other: &Self) -> bool {
        self.hand == other.hand
    }
}

impl Eq for P1Hand {}

impl PartialOrd for P1Hand {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for P1Hand {
    fn cmp(&self, other: &Self) -> Ordering {
        let hand_type_ord = self.hand_type.cmp(&other.hand_type);
        if let Ordering::Equal = hand_type_ord {
            let mut other_iter = other.hand.iter();
            for self_card in self.hand.iter() {
                let other_card = other_iter.next().unwrap();

                let card_ord = self_card.cmp(other_card);
                if let Ordering::Equal = card_ord {
                    continue;
                } else {
                    return card_ord;
                }
            }

            Ordering::Equal
        } else {
            hand_type_ord
        }
    }
}

#[derive(Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
enum P2Card {
    Joker,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Queen,
    King,
    Ace,
}

#[derive(Debug)]
struct P2Hand {
    hand: Box<[P2Card]>,
    hand_type: HandType,
}

impl P2Hand {
    fn new(str: &str) -> Self {
        if str.len() != 5 {
            panic!("Hands must contain exactly 5 cards");
        }

        let hand = str
            .chars()
            .map(|c| match c {
                'A' => P2Card::Ace,
                'K' => P2Card::King,
                'Q' => P2Card::Queen,
                'T' => P2Card::Ten,
                '9' => P2Card::Nine,
                '8' => P2Card::Eight,
                '7' => P2Card::Seven,
                '6' => P2Card::Six,
                '5' => P2Card::Five,
                '4' => P2Card::Four,
                '3' => P2Card::Three,
                '2' => P2Card::Two,
                'J' => P2Card::Joker,
                _ => panic!("{c} is not a valid card"),
            })
            .collect::<Vec<_>>()
            .into_boxed_slice();
        let hand_type = P2Hand::get_type(&hand);

        Self { hand, hand_type }
    }

    fn get_type(cards: &[P2Card]) -> HandType {
        let mut counts = cards.iter().counts();
        if counts.contains_key(&P2Card::Joker) && counts.len() > 1 {
            let joker_counts = counts[&P2Card::Joker];
            for (_, v) in counts.iter_mut().filter(|(&k, _)| k != &P2Card::Joker) {
                *v = (*v + joker_counts).min(5)
            }
            counts.remove(&P2Card::Joker);
        }

        match counts.values().max().unwrap() {
            5 => HandType::Five,
            4 => HandType::Four,
            1 => HandType::High,
            3 => {
                if counts.len() == 2 {
                    HandType::FullHouse
                } else {
                    HandType::Three
                }
            }
            2 => {
                if counts.len() == 3 {
                    HandType::TwoPair
                } else {
                    HandType::OnePair
                }
            }
            _ => panic!("couldn't get hand type for {:?}", counts),
        }
    }
}

impl PartialEq for P2Hand {
    fn eq(&self, other: &Self) -> bool {
        self.hand == other.hand
    }
}

impl Eq for P2Hand {}

impl PartialOrd for P2Hand {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for P2Hand {
    fn cmp(&self, other: &Self) -> Ordering {
        let hand_type_ord = self.hand_type.cmp(&other.hand_type);
        if let Ordering::Equal = hand_type_ord {
            let mut other_iter = other.hand.iter();
            for self_card in self.hand.iter() {
                let other_card = other_iter.next().unwrap();

                let card_ord = self_card.cmp(other_card);
                if let Ordering::Equal = card_ord {
                    continue;
                } else {
                    return card_ord;
                }
            }

            Ordering::Equal
        } else {
            hand_type_ord
        }
    }
}

#[derive(Eq, PartialEq, Ord, PartialOrd, Debug)]
enum HandType {
    High,
    OnePair,
    TwoPair,
    Three,
    FullHouse,
    Four,
    Five,
}

#[cfg(test)]
mod part_1_tests {
    use super::*;

    #[test]
    fn five_of_a_kind() {
        assert!(P1Hand::new("AAAAA").hand_type == HandType::Five);
    }

    #[test]
    fn four_of_a_kind() {
        assert!(P1Hand::new("AA8AA").hand_type == HandType::Four);
    }

    #[test]
    fn full_house() {
        assert!(P1Hand::new("23332").hand_type == HandType::FullHouse);
    }

    #[test]
    fn three_of_a_kind() {
        assert!(P1Hand::new("TTT98").hand_type == HandType::Three);
    }

    #[test]
    fn two_pair() {
        assert!(P1Hand::new("23432").hand_type == HandType::TwoPair);
    }

    #[test]
    fn one_pair() {
        assert!(P1Hand::new("A23A4").hand_type == HandType::OnePair);
    }

    #[test]
    fn high_card() {
        assert!(P1Hand::new("23456").hand_type == HandType::High);
    }

    #[test]
    fn two_pair_is_greater_than_one_pair() {
        assert!(P1Hand::new("23432") > P1Hand::new("A23A4"));
    }

    #[test]
    fn first_card_matters_most() {
        assert!(P1Hand::new("33332") > P1Hand::new("2AAAA"));
        assert!(P1Hand::new("77888") > P1Hand::new("77788"));
    }
}

#[cfg(test)]
mod part_2_tests {
    use super::*;

    #[test]
    fn joker_becomes_best_card() {
        assert!(P2Hand::new("QJJQ2").hand_type == HandType::Four);
    }

    #[test]
    fn joker_is_bad_against_same_type() {
        assert!(P2Hand::new("QQQQ2") > P2Hand::new("JKKK2"));
    }
}

bench!(250602641, 251037509);
