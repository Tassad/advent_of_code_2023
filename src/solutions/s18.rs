use macros::bench;

pub fn part_1(input: &str) -> String {
    let mut max_x: i32 = 0;
    let mut total_x: i32 = 0;
    let mut max_y: i32 = 0;
    let mut total_y: i32 = 0;
    for (direction, steps) in input.lines().map(|s| {
        let mut split = s.split_ascii_whitespace();
        (split.next().unwrap(), split.next().unwrap())
    }) {
        let steps: i32 = steps.parse().unwrap();
        match direction {
            "U" => total_y -= steps,
            "D" => total_y += steps,
            "L" => total_x -= steps,
            "R" => total_x += steps,
            other => unreachable!("input must not contain {other}"),
        }

        if total_x > max_x {
            max_x = total_x;
        }
        if total_y > max_y {
            max_y = total_y;
        }
    }

    let mut grid = Grid::new(
        (max_x + 1).try_into().unwrap(),
        (max_y + 1).try_into().unwrap(),
    );

    let mut pos: (usize, usize) = (0, 0);
    for (direction, steps) in input.lines().map(|s| {
        let mut split = s.split_ascii_whitespace();
        (split.next().unwrap(), split.next().unwrap())
    }) {
        for _ in 0..steps.parse().unwrap() {
            match direction {
                "U" => pos.1 -= 1,
                "D" => pos.1 += 1,
                "L" => pos.0 -= 1,
                "R" => pos.0 += 1,
                other => unreachable!("input must not contain {other}"),
            }
            *grid.get_mut(pos.0, pos.1).unwrap() |= DUG;
        }
    }

    todo!()
}

pub fn part_2(input: &str) -> String {
    todo!()
}

const DUG: u8 = 1 << 0;

#[derive(Clone)]
struct Grid {
    data: Vec<u8>,
    line_offset: usize,
}

impl Grid {
    fn new(width: usize, height: usize) -> Self {
        let line_offset = width;
        let data = std::iter::repeat(0).take(width * height).collect();

        Self { data, line_offset }
    }

    // fn x(&self, i: usize) -> usize {
    //     i % self.line_offset
    // }

    // fn y(&self, i: usize) -> usize {
    //     i / self.line_offset
    // }

    fn height(&self) -> usize {
        self.data.len() / self.line_offset + 1
    }

    // fn get(&self, x: usize, y: usize) -> Option<&u16> {
    //     let i = y * self.line_offset + x;
    //     self.data.get(i)
    // }

    fn get_mut(&mut self, x: usize, y: usize) -> Option<&mut u8> {
        if x >= self.line_offset || y > self.data.len() / self.line_offset {
            return None;
        }
        let i = y.checked_mul(self.line_offset)?.checked_add(x)?;
        self.data.get_mut(i)
    }

    // fn print_layout(&self) {
    //     for (i, char) in self.data.iter().enumerate() {
    //         if char & MIRROR_45_DEGREES != 0 {
    //             print!(r#"/"#);
    //         } else if char & MIRROR_135_DEGREES != 0 {
    //             print!(r#"\"#);
    //         } else if char & SPLITTER_VERTICAL != 0 {
    //             print!(r#"|"#);
    //         } else if char & SPLITTER_HORIZONTAL != 0 {
    //             print!(r#"-"#);
    //         } else {
    //             print!(r#"."#);
    //         }

    //         if i % self.line_offset == self.line_offset - 1 {
    //             println!();
    //         }
    //     }
    //     println!();
    // }
}

impl ::std::fmt::Display for Grid {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for (i, char) in self.data.iter().enumerate() {
            if char & DUG != 0 {
                write!(f, "#")?;
            } else {
                write!(f, ".")?;
            }
            if i % self.line_offset == self.line_offset - 1 {
                writeln!(f)?;
            }
        }

        Ok(())
    }
}

#[cfg(test)]
mod sample_tests {
    use super::*;

    const SAMPLE_INPUTS: [&str; 1] = ["\
R 6 (#70c710)
D 5 (#0dc571)
L 2 (#5713f0)
D 2 (#d2c081)
R 2 (#59c680)
D 2 (#411b91)
L 5 (#8ceee2)
U 2 (#caa173)
L 1 (#1b58a2)
U 2 (#caa171)
R 2 (#7807d2)
U 3 (#a77fa3)
L 2 (#015232)
U 2 (#7a21e3)
"];

    #[test]
    fn part_1_works_on_samples() {
        assert_eq!(part_1(SAMPLE_INPUTS[0]), "62");
    }

    #[test]
    fn part_2_works_on_samples() {
        // assert_eq!(part_2(SAMPLE_INPUTS[0]), "51");
    }
}

// bench!(,);
