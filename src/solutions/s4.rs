use std::collections::VecDeque;

use macros::bench;

pub fn part_1(input: &str) -> String {
    input
        .lines()
        .map(|line| {
            let winning_numbers = line
                .split_ascii_whitespace()
                .skip(2)
                .take_while(|&s| s != "|")
                .collect::<Vec<_>>();
            let drawn_numbers = line
                .split_ascii_whitespace()
                .skip_while(|&s| s != "|")
                .skip(1);

            drawn_numbers.fold(0, |acc, drawn| {
                if winning_numbers.iter().any(|&winning| drawn == winning) {
                    if acc != 0 {
                        acc << 1
                    } else {
                        1
                    }
                } else {
                    acc
                }
            })
        })
        .sum::<u32>()
        .to_string()
}

pub fn part_2(input: &str) -> String {
    let mut next_counts: VecDeque<u32> = VecDeque::with_capacity(200);

    input
        .lines()
        .map(|line| {
            let winning_numbers = line
                .split_ascii_whitespace()
                .skip(2)
                .take_while(|&s| s != "|")
                .collect::<Vec<_>>();
            let drawn_numbers = line
                .split_ascii_whitespace()
                .skip_while(|&s| s != "|")
                .skip(1);

            let instances = 1 + next_counts.pop_front().unwrap_or_default();

            let next_n: u32 = drawn_numbers.fold(0, |acc, drawn| {
                if winning_numbers.iter().any(|&winning| drawn == winning) {
                    acc + 1
                } else {
                    acc
                }
            });
            while next_counts.len() < next_n as usize {
                next_counts.push_back(0);
            }
            for count in next_counts.iter_mut().take(next_n as usize) {
                *count += instances;
            }

            instances
        })
        .sum::<u32>()
        .to_string()
}

bench!(21213, 8549735);
