use itertools::Itertools;
use macros::bench;

pub fn part_1(input: &str) -> String {
    let mut grid = Grid::new(input);
    let rolling_rocks = grid
        .data
        .iter()
        .enumerate()
        .filter(|(_, c)| c == &&'O')
        .map(|(i, _)| (grid.x(i), grid.y(i)))
        .collect::<Vec<_>>();

    for (start_x, start_y) in rolling_rocks.into_iter() {
        for y in (0..start_y).rev() {
            if let Some(space) = grid.get_mut(start_x, y) {
                if space == &mut '.' {
                    *space = 'O';
                    *grid.get_mut(start_x, y + 1).unwrap() = '.';
                } else {
                    break;
                }
            }
        }
    }

    grid.data
        .iter()
        .enumerate()
        .filter(|(_, c)| c == &&'O')
        .map(|(i, _)| grid.data.len() / grid.line_offset - grid.y(i))
        .sum::<usize>()
        .to_string()
}

pub fn part_2(input: &str) -> String {
    let mut grid = Grid::new(input);
    let mut grid_history = vec![grid.clone()];

    let mut rolling_rocks = grid
        .data
        .iter()
        .enumerate()
        .filter(|(_, c)| c == &&'O')
        .map(|(i, _)| (grid.x(i), grid.y(i)))
        .collect::<Vec<_>>();

    for (start_x, start_y) in rolling_rocks.iter_mut() {
        for y in (0..*start_y).rev() {
            if let Some(space) = grid.get_mut(*start_x, y) {
                if space == &mut '.' {
                    *space = 'O';
                    *grid.get_mut(*start_x, y + 1).unwrap() = '.';
                    *start_y = y;
                } else {
                    *start_y = y + 1;
                    break;
                }
            }
        }
    }
    rolling_rocks.sort_unstable();
    for (start_x, start_y) in rolling_rocks.iter_mut() {
        for x in (0..*start_x).rev() {
            if let Some(space) = grid.get_mut(x, *start_y) {
                if space == &mut '.' {
                    *space = 'O';
                    *grid.get_mut(x + 1, *start_y).unwrap() = '.';
                    *start_x = x;
                } else {
                    *start_x = x + 1;
                    break;
                }
            }
        }
    }
    rolling_rocks.sort_unstable();

    println!("{grid}");
    let test_rocks = grid
        .data
        .iter()
        .enumerate()
        .filter(|(_, c)| c == &&'O')
        .map(|(i, _)| (grid.x(i), grid.y(i)))
        .sorted_unstable()
        .collect::<Vec<_>>();
    rolling_rocks.sort_unstable();
    assert_eq!(test_rocks, rolling_rocks);

    for (start_x, start_y) in rolling_rocks.iter_mut() {
        for y in *start_y + 1..grid.height() {
            if let Some(space) = grid.get_mut(*start_x, y) {
                if space == &mut '.' {
                    *space = 'O';
                    *grid.get_mut(*start_x, y - 1).unwrap() = '.';
                    *start_y = y;
                } else {
                    *start_y = y - 1;
                    break;
                }
            }
        }
    }
    rolling_rocks.sort_unstable();

    println!("{grid}");
    let test_rocks = grid
        .data
        .iter()
        .enumerate()
        .filter(|(_, c)| c == &&'O')
        .map(|(i, _)| (grid.x(i), grid.y(i)))
        .sorted_unstable()
        .collect::<Vec<_>>();
    rolling_rocks.sort_unstable();
    assert_eq!(test_rocks, rolling_rocks);

    for (start_x, start_y) in rolling_rocks.iter_mut() {
        for x in *start_x + 1..grid.line_offset {
            if let Some(space) = grid.get_mut(x, *start_y) {
                if space == &mut '.' {
                    *space = 'O';
                    *grid.get_mut(x - 1, *start_y).unwrap() = '.';
                    *start_x = x;
                } else {
                    *start_x = x - 1;
                    break;
                }
            }
        }
    }
    rolling_rocks.sort_unstable();

    println!("{grid}");
    let test_rocks = grid
        .data
        .iter()
        .enumerate()
        .filter(|(_, c)| c == &&'O')
        .map(|(i, _)| (grid.x(i), grid.y(i)))
        .sorted_unstable()
        .collect::<Vec<_>>();
    rolling_rocks.sort_unstable();
    assert_eq!(test_rocks, rolling_rocks);

    todo!();
    // grid.data
    //     .iter()
    //     .enumerate()
    //     .filter(|(_, c)| c == &&'O')
    //     .map(|(i, _)| grid.data.len() / grid.line_offset - grid.y(i))
    //     .sum::<usize>()
    //     .to_string()
}

#[derive(Clone)]
struct Grid {
    data: Vec<char>,
    line_offset: usize,
}

impl Grid {
    fn new(s: &str) -> Self {
        let mut lines = s.lines().peekable();

        let line_offset = lines.peek().map_or(0, |line| line.len());
        let data = lines.flat_map(|s| s.chars()).collect::<Vec<_>>();

        Self { data, line_offset }
    }

    fn x(&self, i: usize) -> usize {
        i % self.line_offset
    }

    fn y(&self, i: usize) -> usize {
        i / self.line_offset
    }

    fn height(&self) -> usize {
        self.data.len() / self.line_offset + 1
    }

    fn get(&self, x: usize, y: usize) -> Option<&char> {
        let i = y * self.line_offset + x;
        self.data.get(i)
    }

    fn get_mut(&mut self, x: usize, y: usize) -> Option<&mut char> {
        let i = y * self.line_offset + x;
        self.data.get_mut(i)
    }
}

impl ::std::fmt::Display for Grid {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for (i, char) in self.data.iter().enumerate() {
            write!(f, "{char}")?;
            if i % self.line_offset == self.line_offset - 1 {
                writeln!(f)?;
            }
        }

        Ok(())
    }
}

#[cfg(test)]
mod sample_tests {
    use super::*;

    const SAMPLE_INPUTS: [&str; 1] = ["\
O....#....
O.OO#....#
.....##...
OO.#O....O
.O.....O#.
O.#..O.#.#
..O..#O..O
.......O..
#....###..
#OO..#....
"];

    #[test]
    fn part_1_works_on_samples() {
        assert_eq!(part_1(SAMPLE_INPUTS[0]), "136");
    }

    #[test]
    fn part_2_works_on_samples() {
        assert_eq!(part_2(SAMPLE_INPUTS[0]), "64");
    }
}

// bench!(106997,);
