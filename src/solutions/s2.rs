use macros::bench;

const RED_LIMIT: u32 = 12;
const GREEN_LIMIT: u32 = 13;
const BLUE_LIMIT: u32 = 14;

pub fn part_1(input: &str) -> String {
    input
        .lines()
        .map(|line| {
            let mut chars = line.chars().skip(5);
            let id = (&mut chars)
                .take_while(char::is_ascii_digit)
                .collect::<String>();

            let mut num = String::default();
            let mut colour = String::default();
            for c in chars.skip(1) {
                if !c.is_alphanumeric() {
                    if !colour.is_empty() && !num.is_empty() {
                        if let Some(n) = is_over_limit(&num, &colour) {
                            return n;
                        }

                        num = String::default();
                        colour = String::default();
                    }

                    continue;
                }

                if c.is_ascii_digit() {
                    num.push(c);
                }

                if c.is_ascii_alphabetic() {
                    colour.push(c);
                }
            }

            if let Some(n) = is_over_limit(&num, &colour) {
                return n;
            }

            id.parse::<u32>().unwrap()
        })
        .sum::<u32>()
        .to_string()
}

pub fn part_2(input: &str) -> String {
    input
        .lines()
        .map(|line| {
            let chars = line.chars().skip(5).skip_while(char::is_ascii_digit);

            let mut num = String::default();
            let mut colour = String::default();
            let mut biggest_red = u32::default();
            let mut biggest_green = u32::default();
            let mut biggest_blue = u32::default();
            for c in chars.skip(1) {
                if !c.is_alphanumeric() {
                    if !colour.is_empty() && !num.is_empty() {
                        new_biggest(
                            &num,
                            &colour,
                            &mut biggest_red,
                            &mut biggest_green,
                            &mut biggest_blue,
                        );

                        num = String::default();
                        colour = String::default();
                    }

                    continue;
                }

                if c.is_ascii_digit() {
                    num.push(c);
                }

                if c.is_ascii_alphabetic() {
                    colour.push(c);
                }
            }

            new_biggest(
                &num,
                &colour,
                &mut biggest_red,
                &mut biggest_green,
                &mut biggest_blue,
            );

            if biggest_red == u32::default()
                || biggest_green == u32::default()
                || biggest_blue == u32::default()
            {
                dbg!(biggest_red);
                dbg!(biggest_green);
                dbg!(biggest_blue);
                panic!();
            }

            biggest_red * biggest_green * biggest_blue
        })
        .sum::<u32>()
        .to_string()
}

fn is_over_limit(num: &str, colour: &str) -> Option<u32> {
    match colour {
        "red" => {
            if num.parse::<u32>().unwrap() > RED_LIMIT {
                return Some(0);
            }
        }
        "green" => {
            if num.parse::<u32>().unwrap() > GREEN_LIMIT {
                return Some(0);
            }
        }
        "blue" => {
            if num.parse::<u32>().unwrap() > BLUE_LIMIT {
                return Some(0);
            }
        }
        other => panic!("{other} is not a creative colour"),
    }

    None
}

fn new_biggest(num: &str, colour: &str, red: &mut u32, green: &mut u32, blue: &mut u32) {
    match colour {
        "red" => {
            let current_reds = num.parse::<u32>().unwrap();
            if current_reds > *red {
                *red = current_reds;
            }
        }
        "green" => {
            let current_greens = num.parse::<u32>().unwrap();
            if current_greens > *green {
                *green = current_greens;
            }
        }
        "blue" => {
            let current_blues = num.parse::<u32>().unwrap();
            if current_blues > *blue {
                *blue = current_blues;
            }
        }
        other => panic!("{other} is not a creative colour"),
    }
}

bench!(2771, 70924);
