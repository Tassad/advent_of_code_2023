use std::iter::repeat;

use macros::bench;

pub fn part_1(input: &str) -> String {
    let grid = Grid::new(input);

    energize_grid(grid, ((-1, 0), Direction::Right)).to_string()
}

pub fn part_2(input: &str) -> String {
    let grid = Grid::new(input);
    let starts_from_left = repeat(-1)
        .zip(0..grid.height() as i32)
        .zip(repeat(Direction::Right));
    let starts_from_up = (0..grid.line_offset as i32)
        .zip(repeat(-1))
        .zip(repeat(Direction::Down));
    let starts_from_right = repeat(grid.line_offset as i32)
        .zip(0..grid.height() as i32)
        .zip(repeat(Direction::Left));
    let starts_from_down = (0..grid.line_offset as i32)
        .zip(repeat(grid.height() as i32))
        .zip(repeat(Direction::Up));

    starts_from_left
        .chain(starts_from_up)
        .chain(starts_from_right)
        .chain(starts_from_down)
        .fold(0, |acc, start| {
            let new = energize_grid(grid.clone(), start);

            if new > acc {
                new
            } else {
                acc
            }
        })
        .to_string()
}

fn energize_grid(mut grid: Grid, start: ((i32, i32), Direction)) -> usize {
    let mut movements = vec![start];

    while let Some(((x, y), direction)) = movements.pop() {
        let (next_x, next_y) = match direction {
            Direction::Up => (x, y - 1),
            Direction::Down => (x, y + 1),
            Direction::Left => (x - 1, y),
            Direction::Right => (x + 1, y),
        };

        if let Some(tile) = grid.get_mut(next_x as usize, next_y as usize) {
            let keep_going = match direction {
                Direction::Up => *tile & BEAM_UP == 0,
                Direction::Down => *tile & BEAM_DOWN == 0,
                Direction::Left => *tile & BEAM_LEFT == 0,
                Direction::Right => *tile & BEAM_RIGHT == 0,
            };

            *tile |= ENERGIZED;
            match direction {
                Direction::Up => *tile |= BEAM_UP,
                Direction::Down => *tile |= BEAM_DOWN,
                Direction::Left => *tile |= BEAM_LEFT,
                Direction::Right => *tile |= BEAM_RIGHT,
            }

            if keep_going {
                let mut next_directions = Vec::with_capacity(2);
                if *tile & MIRROR_45_DEGREES != 0 {
                    next_directions.push(match direction {
                        Direction::Up => Direction::Right,
                        Direction::Down => Direction::Left,
                        Direction::Left => Direction::Down,
                        Direction::Right => Direction::Up,
                    });
                } else if *tile & MIRROR_135_DEGREES != 0 {
                    next_directions.push(match direction {
                        Direction::Up => Direction::Left,
                        Direction::Down => Direction::Right,
                        Direction::Left => Direction::Up,
                        Direction::Right => Direction::Down,
                    });
                } else if *tile & SPLITTER_VERTICAL != 0 {
                    match direction {
                        Direction::Up | Direction::Down => next_directions.push(direction),
                        Direction::Left | Direction::Right => {
                            next_directions.push(Direction::Up);
                            next_directions.push(Direction::Down);
                        }
                    }
                } else if *tile & SPLITTER_HORIZONTAL != 0 {
                    match direction {
                        Direction::Left | Direction::Right => next_directions.push(direction),
                        Direction::Up | Direction::Down => {
                            next_directions.push(Direction::Left);
                            next_directions.push(Direction::Right);
                        }
                    }
                } else {
                    next_directions.push(direction);
                }

                while let Some(next_direction) = next_directions.pop() {
                    movements.push(((next_x, next_y), next_direction));
                }
            }
        }
    }

    grid.data.into_iter().filter(|t| t & ENERGIZED != 0).count()
}

#[derive(PartialEq, Clone, Copy)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

const ENERGIZED: u16 = 1 << 0;
const MIRROR_45_DEGREES: u16 = 1 << 1;
const MIRROR_135_DEGREES: u16 = 1 << 2;
const SPLITTER_VERTICAL: u16 = 1 << 3;
const SPLITTER_HORIZONTAL: u16 = 1 << 4;
const BEAM_UP: u16 = 1 << 5;
const BEAM_DOWN: u16 = 1 << 6;
const BEAM_LEFT: u16 = 1 << 7;
const BEAM_RIGHT: u16 = 1 << 8;

#[derive(Clone)]
struct Grid {
    data: Vec<u16>,
    line_offset: usize,
}

impl Grid {
    fn new(s: &str) -> Self {
        let mut lines = s.lines().peekable();

        let line_offset = lines.peek().map_or(0, |line| line.len());
        let data = lines
            .flat_map(|s| {
                s.chars().map(|c| match c {
                    '.' => 0,
                    '/' => MIRROR_45_DEGREES,
                    '\\' => MIRROR_135_DEGREES,
                    '|' => SPLITTER_VERTICAL,
                    '-' => SPLITTER_HORIZONTAL,
                    _ => unreachable!("input must not contain {c}"),
                })
            })
            .collect::<Vec<_>>();

        Self { data, line_offset }
    }

    // fn x(&self, i: usize) -> usize {
    //     i % self.line_offset
    // }

    // fn y(&self, i: usize) -> usize {
    //     i / self.line_offset
    // }

    fn height(&self) -> usize {
        self.data.len() / self.line_offset + 1
    }

    // fn get(&self, x: usize, y: usize) -> Option<&u16> {
    //     let i = y * self.line_offset + x;
    //     self.data.get(i)
    // }

    fn get_mut(&mut self, x: usize, y: usize) -> Option<&mut u16> {
        if x >= self.line_offset || y > self.data.len() / self.line_offset {
            return None;
        }
        let i = y.checked_mul(self.line_offset)?.checked_add(x)?;
        self.data.get_mut(i)
    }

    // fn print_layout(&self) {
    //     for (i, char) in self.data.iter().enumerate() {
    //         if char & MIRROR_45_DEGREES != 0 {
    //             print!(r#"/"#);
    //         } else if char & MIRROR_135_DEGREES != 0 {
    //             print!(r#"\"#);
    //         } else if char & SPLITTER_VERTICAL != 0 {
    //             print!(r#"|"#);
    //         } else if char & SPLITTER_HORIZONTAL != 0 {
    //             print!(r#"-"#);
    //         } else {
    //             print!(r#"."#);
    //         }

    //         if i % self.line_offset == self.line_offset - 1 {
    //             println!();
    //         }
    //     }
    //     println!();
    // }
}

impl ::std::fmt::Display for Grid {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for (i, char) in self.data.iter().enumerate() {
            if char & ENERGIZED != 0 {
                write!(f, "#")?;
            } else {
                write!(f, ".")?;
            }
            if i % self.line_offset == self.line_offset - 1 {
                writeln!(f)?;
            }
        }

        Ok(())
    }
}

#[cfg(test)]
mod sample_tests {
    use super::*;

    const SAMPLE_INPUTS: [&str; 1] = ["\
.|...\\....
|.-.\\.....
.....|-...
........|.
..........
.........\\
..../.\\\\..
.-.-/..|..
.|....-|.\\
..//.|....
"];

    #[test]
    fn part_1_works_on_samples() {
        assert_eq!(part_1(SAMPLE_INPUTS[0]), "46");
    }

    #[test]
    fn part_2_works_on_samples() {
        assert_eq!(part_2(SAMPLE_INPUTS[0]), "51");
    }
}

bench!(6795, 7154);
