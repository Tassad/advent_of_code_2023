use itertools::Itertools;

use macros::bench;

pub fn part_1(input: &str) -> String {
    let mut grid = Grid::new(input);

    let mut empty_rows = Vec::new();
    for (i, row) in grid.rows_iter().enumerate() {
        if row.iter().any(|p| p & GALAXY != 0) {
            continue;
        }

        empty_rows.push(i + empty_rows.len());
    }

    let mut empty_cols = Vec::new();
    for x in 0..grid.line_offset {
        if grid.col_iter(x).any(|p| p & GALAXY != 0) {
            continue;
        }

        empty_cols.push(x + empty_cols.len());
    }

    for y in empty_rows {
        grid.double_row(y);
    }
    for x in empty_cols {
        grid.double_col(x);
    }

    grid.points
        .iter()
        .enumerate()
        .filter(|(_, &p)| p & GALAXY == GALAXY)
        .map(|(i, _)| i)
        .combinations(2)
        .map(|pair| {
            (pair[1] / grid.line_offset).abs_diff(pair[0] / grid.line_offset)
                + (pair[1] % grid.line_offset).abs_diff(pair[0] % grid.line_offset)
        })
        .sum::<usize>()
        .to_string()
}

pub fn part_2(input: &str) -> String {
    let grid = Grid::new(input);

    let mut empty_rows = Vec::new();
    for (i, row) in grid.rows_iter().enumerate() {
        if row.iter().any(|p| p & GALAXY != 0) {
            continue;
        }

        empty_rows.push(i);
    }

    let mut empty_cols = Vec::new();
    for x in 0..grid.line_offset {
        if grid.col_iter(x).any(|p| p & GALAXY != 0) {
            continue;
        }

        empty_cols.push(x);
    }

    grid.points
        .iter()
        .enumerate()
        .filter(|(_, &p)| p & GALAXY == GALAXY)
        .map(|(i, _)| i)
        .combinations(2)
        .map(|pair| {
            let (start_x, start_y) = (pair[0] % grid.line_offset, pair[0] / grid.line_offset);
            let (end_x, end_y) = (pair[1] % grid.line_offset, pair[1] / grid.line_offset);
            let mut distance = end_y.abs_diff(start_y) + end_x.abs_diff(start_x);

            for row in empty_rows.iter() {
                if (end_y.min(start_y) + 1..end_y.max(start_y)).contains(row) {
                    distance += 999_999;
                }
            }

            for col in empty_cols.iter() {
                if (end_x.min(start_x) + 1..end_x.max(start_x)).contains(col) {
                    distance += 999_999;
                }
            }

            distance
        })
        .sum::<usize>()
        .to_string()
}

const NOTHING: u8 = 0;
const GALAXY: u8 = 1 << 0;
// const VISITED: u8 = 1 << 1;

#[derive(Clone)]
struct Grid {
    points: Vec<u8>,
    line_offset: usize,
}

impl Grid {
    fn new(s: &str) -> Self {
        let mut lines = s.lines().peekable();
        let line_offset = lines.peek().map_or(0, |line| line.len());

        Self {
            points: lines
                .flat_map(|line| {
                    line.chars().map(|c| match c {
                        '.' => NOTHING,
                        '#' => GALAXY,
                        other => unreachable!("input must not contain {other}"),
                    })
                })
                .collect(),
            line_offset,
        }
    }

    // fn get(&self, x: usize, y: usize) -> Option<&u8> {
    //     self.points.get(y * self.line_offset + x)
    // }

    // fn get_mut(&mut self, x: usize, y: usize) -> Option<&mut u8> {
    //     self.points.get_mut(y * self.line_offset + x)
    // }

    // fn get_adjacent(&self, x: usize, y: usize) -> Vec<(usize, usize)> {
    //     let mut out = vec![];

    //     for x in [x.checked_sub(1), Some(x + 1)].into_iter().flatten() {
    //         out.push((x, y));
    //     }

    //     for y in [y.checked_sub(1), Some(y + 1)].into_iter().flatten() {
    //         out.push((x, y));
    //     }

    //     out
    // }

    fn double_row(&mut self, y: usize) {
        let new_line_at = (y + 1) * self.line_offset;
        let new_line = std::iter::repeat(0).take(new_line_at - y * self.line_offset);

        self.points.splice(new_line_at..new_line_at, new_line);
    }

    fn double_col(&mut self, x: usize) {
        let line_count = self.points.len() / self.line_offset;

        let insert_indices = {
            let mut offset: usize = 0;
            let line_offset = self.line_offset;
            ::std::iter::from_fn(move || {
                if offset >= line_count {
                    None
                } else {
                    offset += 1;
                    Some(x + (offset - 1) * line_offset + (offset - 1))
                }
            })
        };

        for i in insert_indices {
            self.points.insert(i, 0);
        }

        self.line_offset += 1;
    }

    fn rows_iter(&self) -> impl Iterator<Item = &[u8]> {
        let mut i = 0;
        ::std::iter::from_fn(move || {
            i += 1;
            if (i - 1) * self.line_offset < self.points.len() {
                Some(&self.points[(i - 1) * self.line_offset..i * self.line_offset])
            } else {
                None
            }
        })
    }

    fn col_iter(&self, x: usize) -> impl Iterator<Item = &u8> {
        let mut i = 0;
        ::std::iter::from_fn(move || {
            i += 1;
            if (i - 1) * self.line_offset < self.points.len() {
                Some(&self.points[x + (i - 1) * self.line_offset])
            } else {
                None
            }
        })
    }
}

impl ::std::fmt::Display for Grid {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for (i, point) in self.points.iter().enumerate() {
            write!(f, "{point}")?;
            if i % self.line_offset == self.line_offset - 1 {
                writeln!(f)?;
            }
        }

        Ok(())
    }
}

#[cfg(test)]
mod sample_tests {
    use super::*;

    const SAMPLE_INPUTS: [&str; 1] = ["\
...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....
"];

    #[test]
    fn part_1_works_on_samples() {
        assert_eq!(part_1(SAMPLE_INPUTS[0]), "374");
    }
}

bench!(9556712, 678626199476_u64);
