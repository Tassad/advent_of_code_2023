use itertools::Itertools;
use rust_decimal::prelude::*;

use macros::bench;

pub fn part_1(input: &str) -> String {
    let times = input
        .lines()
        .next()
        .unwrap()
        .split_ascii_whitespace()
        .skip(1);
    let mut distances = input
        .lines()
        .nth(1)
        .unwrap()
        .split_ascii_whitespace()
        .skip(1);

    times
        .fold(0, |acc, time| {
            let time = time.parse::<i32>().unwrap();
            let distance = distances.next().unwrap().parse::<i32>().unwrap();
            let derivative_zero = time / 2;

            let first_win = ((time as f32 - ((-time * -time - 4 * (distance + 1)) as f32).sqrt())
                / 2.)
                .ceil() as _;

            if derivative_zero < first_win {
                return acc;
            }

            let mut wins = (derivative_zero - first_win + 1) * 2;
            if time % 2 == 0 {
                wins -= 1;
            }
            if acc == 0 {
                wins
            } else {
                acc * wins
            }
        })
        .to_string()
}

pub fn part_2(input: &str) -> String {
    let time = Decimal::from_str(
        &input
            .lines()
            .next()
            .unwrap()
            .split_ascii_whitespace()
            .skip(1)
            .join(""),
    )
    .unwrap();
    let distance = Decimal::from_str(
        &input
            .lines()
            .nth(1)
            .unwrap()
            .split_ascii_whitespace()
            .skip(1)
            .join(""),
    )
    .unwrap();

    let derivative_zero: Decimal = (time / Decimal::from(2)).floor();

    let first_win: Decimal = ((time
        - (-time * -time - Decimal::from(4) * (distance + Decimal::from(1)))
            .sqrt()
            .unwrap())
        / Decimal::from(2))
    .ceil();

    if derivative_zero < first_win {
        return 0.to_string();
    }

    let mut wins: Decimal = (derivative_zero - first_win + Decimal::from(1)) * Decimal::from(2);

    if time % Decimal::from(2) == Decimal::from(0) {
        wins -= Decimal::from(1);
    }

    wins.round_dp(0).to_string()
}

bench!(220320, 34454850);
