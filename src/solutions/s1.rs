use std::collections::HashMap;

use itertools::Itertools;
use itertools::MinMaxResult;

use macros::bench;

pub fn part_1(input: &str) -> String {
    let mut sum = 0;
    for line in input.lines() {
        sum += line
            .chars()
            .fold([None; 2], |mut acc: [Option<char>; 2], c| {
                if c.is_ascii_digit() {
                    if acc[0].is_none() {
                        acc[0] = Some(c);
                    }
                    acc[1] = Some(c);
                }
                acc
            })
            .into_iter()
            .flatten()
            .collect::<String>()
            .parse::<u32>()
            .unwrap();
    }

    sum.to_string()
}

pub fn part_2(input: &str) -> String {
    let numbers: HashMap<&str, &str> = HashMap::from([
        ("one", "1"),
        ("two", "2"),
        ("three", "3"),
        ("four", "4"),
        ("five", "5"),
        ("six", "6"),
        ("seven", "7"),
        ("eight", "8"),
        ("nine", "9"),
    ]);

    let mut sum: u32 = 0;
    for line in input.lines() {
        let minmax = numbers
            .keys()
            .chain(numbers.values())
            .filter_map(|s| Some((line.find(s)?, s)))
            .minmax_by(|a, b| a.0.cmp(&b.0));

        let (min, max) = match minmax {
            MinMaxResult::MinMax(min, max) => (
                numbers.get(min.1).unwrap_or(min.1),
                numbers.get(max.1).unwrap_or(max.1),
            ),
            MinMaxResult::OneElement(minmax) => {
                let minmax = numbers.get(minmax.1).unwrap_or(minmax.1);
                (minmax, minmax)
            }
            MinMaxResult::NoElements => panic!(),
        };

        sum += (min.to_string() + max).parse::<u32>().unwrap();
    }

    sum.to_string()
}

bench!(55208, 54578);
