use macros::bench;

pub fn part_1(input: &str) -> String {
    let mut row = Row::new(input.lines().next().unwrap());

    println!("{:?}", row);

    todo!();
    // input.lines().map(|line| {
    //     let row = Row::new(line);
    // })
    // .sum::<u32>()
    // .to_string()
}

pub fn part_2(input: &str) -> String {
    todo!();
}

#[derive(PartialEq, Debug)]
enum Spring {
    Operational,
    Damaged,
    Any,
}

#[derive(Debug)]
struct Row {
    springs: Vec<Spring>,
    groups: Vec<usize>,
}

impl Row {
    fn new(line: &str) -> Self {
        let mut split = line.split_ascii_whitespace();
        let mut springs = split
            .next()
            .unwrap()
            .chars()
            .map(|c| match c {
                '.' => Spring::Operational,
                '#' => Spring::Damaged,
                '?' => Spring::Any,
                other => unreachable!("input must not contain {other}"),
            })
            .collect::<Vec<_>>();
        let mut groups = split
            .next()
            .unwrap()
            .split(',')
            .map(|s| s.parse::<usize>().unwrap())
            .collect::<Vec<_>>();

        let mut full_groups = Vec::new();
        'group: for group_len in groups.iter_mut() {
            let mut spring_iter = springs.iter().enumerate();
            while let Some((i, spring)) = spring_iter.next() {
                if *group_len > springs.len() - i {
                    break;
                }
                if spring == &Spring::Damaged
                    && springs[i + 1..i + *group_len]
                        .iter()
                        .all(|sp| matches!(sp, Spring::Any | Spring::Damaged))
                    && springs
                        .get(i + *group_len + 1)
                        .map_or(true, |sp| sp != &Spring::Damaged)
                    && (i.checked_sub(1).is_none() || springs[i - 1] != Spring::Damaged)
                {
                    full_groups.push((i, *group_len));
                    spring_iter.nth(*group_len - 1);
                    *group_len = 0;
                    continue 'group;
                }
            }
        }
        groups = groups.iter().copied().filter(|n| n > &0).collect();
        let mut offset = 0;
        for (i, len) in full_groups {
            springs.splice(i - 1 - offset..(i + len - offset).min(springs.len()), []);
            offset += len;
        }

        Self { springs, groups }
    }
}

#[cfg(test)]
mod sample_tests {
    use super::*;

    const SAMPLE_INPUTS: [&str; 1] = ["\
?#?#?#?#?#?#?#? 1,3,1,6
???.### 1,1,3
.??..??...?##. 1,1,3
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1
"];

    #[test]
    fn part_1_works_on_samples() {
        assert_eq!(part_1(SAMPLE_INPUTS[0]), "21");
    }

    #[test]
    fn part_2_works_on_samples() {
        todo!();
    }
}

// bench!(,);
