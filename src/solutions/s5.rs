use itertools::Itertools;

use macros::bench;

pub fn part_1(input: &str) -> String {
    let seeds = input
        .split("\n\n")
        .next()
        .unwrap()
        .split_ascii_whitespace()
        .skip(1);

    seeds
        .fold(u64::MAX, |acc, seed| {
            let mut num = seed.parse().unwrap();
            for category in input.split("\n\n").skip(1) {
                new_num(&mut num, category.lines().skip(1));
            }

            num.min(acc)
        })
        .to_string()
}

pub fn part_2(input: &str) -> String {
    let chunks = input
        .split("\n\n")
        .next()
        .unwrap()
        .split_ascii_whitespace()
        .skip(1)
        .chunks(2);
    let mut starts = chunks
        .into_iter()
        .map(|el| {
            let (start, length) = el
                .map(|s| s.parse::<u64>().unwrap())
                .collect_tuple()
                .unwrap();

            (start, start + length - 1)
        })
        .collect::<Vec<_>>();

    for category in input.split("\n\n").skip(1) {
        let mut tos = Vec::new();

        'starts: while let Some(start_range) = starts.pop() {
            for line in category.lines().skip(1) {
                let (dest, source, length) = line
                    .split_ascii_whitespace()
                    .map(|s| s.parse::<u64>().unwrap())
                    .collect_tuple()
                    .unwrap();

                let (source_start, source_end) = (source, source + length - 1);

                if source_start <= start_range.1 && source_end >= start_range.0 {
                    if start_range.0 < source_start {
                        starts.push((start_range.0, source_start - 1));
                    }

                    if start_range.1 > source_end {
                        starts.push((source_end + 1, start_range.1));
                    }

                    tos.push((
                        source_start.max(start_range.0) - source_start + dest,
                        source_end.min(start_range.1) - source_start + dest,
                    ));

                    continue 'starts;
                }
            }

            tos.push((start_range.0, start_range.1));
        }

        starts = tos;
    }

    starts
        .iter()
        .min_by(|a, b| a.0.cmp(&b.0))
        .unwrap()
        .0
        .to_string()
}

fn new_num<'a>(num: &mut u64, it: impl Iterator<Item = &'a str>) {
    for line in it {
        let (dest, source, length) = line
            .split_ascii_whitespace()
            .map(|s| s.parse::<u64>().unwrap())
            .collect_tuple()
            .unwrap();

        if (source..(source + length)).contains(num) {
            *num = *num - source + dest;
            return;
        }
    }
}

bench!(157211394, 50855035);
