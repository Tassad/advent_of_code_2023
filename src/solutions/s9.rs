use macros::bench;

pub fn part_1(input: &str) -> String {
    input
        .lines()
        .map(|history| {
            let mut last_numbers = Vec::new();
            let mut latest_sequence = history
                .split_ascii_whitespace()
                .map(|n| n.parse::<i32>().unwrap())
                .collect::<Vec<_>>();
            last_numbers.push(*latest_sequence.last().unwrap());
            while last_numbers.last().is_some_and(|&last| last != 0) {
                let mut new_latest = Vec::with_capacity(latest_sequence.len() - 1);
                for window in latest_sequence.windows(2) {
                    new_latest.push(window[1] - window[0]);
                }

                last_numbers.push(*new_latest.last().unwrap());
                latest_sequence = new_latest;
            }

            last_numbers.into_iter().sum::<i32>()
        })
        .sum::<i32>()
        .to_string()
}

pub fn part_2(input: &str) -> String {
    input
        .lines()
        .map(|history| {
            let mut first_numbers = Vec::new();
            let mut latest_sequence = history
                .split_ascii_whitespace()
                .map(|n| n.parse::<i32>().unwrap())
                .collect::<Vec<_>>();
            first_numbers.push(*latest_sequence.first().unwrap());
            while !latest_sequence.iter().all(|n| n == &0) {
                let mut new_latest = Vec::with_capacity(latest_sequence.len() - 1);
                for window in latest_sequence.windows(2) {
                    new_latest.push(window[1] - window[0]);
                }

                first_numbers.push(*new_latest.first().unwrap());
                latest_sequence = new_latest;
            }

            first_numbers.into_iter().rfold(0, |acc, n| n - acc)
        })
        .sum::<i32>()
        .to_string()
}

bench!(1757008019, 995);
